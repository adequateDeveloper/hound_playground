defmodule HoundPlayground.LoginTest do
  use HoundPlayground.ConnCase  # access to app infrastructure, i.e. path helpers
  use Hound.Helpers

@docp """
The test can be broken down into the following steps:

1. Visit http://localhost:4000/login,
2. Locate the username field,
3. Fill it in with a test value (e.g. "John"),
4. Locate the submit button,
5. Click the submit button,
6. Locate the error message container,
7. Get its text,
8. Check if the text equals 'Your password is invalid!', and
9. Check if we’re still on the login page.
"""

@docp """
The hound_session manages the session's lifecycle for us: it creates and ends
a new session for each test, so they don’t interfere.
Without it, we would have to use start_session/1 / end_session/1 helpers in each test.
"""
  hound_session()

  # retrieve the login URL using path helpers
  defp login_index do
    login_url(HoundPlayground.Endpoint, :index)
  end

  defp alert_danger_text do
    alert = find_element(:xpath, ~s|//p[contains(@class, 'alert-danger')]|)
    visible_text(alert)
  end

  test "invalid username" do
    login_index() |> navigate_to()

    form = find_element(:id, "login")
    username = find_within_element(form, :id, "username")
    submit = find_within_element(form, :class, "btn-lg")

    username |> fill_field("john")
    submit |> click()
    take_screenshot()

    # alternative solution:
    # alert = find_all_elements(:tag, "p") |> Enum.find(&(has_class?(&1, "alert-danger")))

    # alert = find_element(:xpath, ~s|//p[contains(@class, 'alert-danger')]|)
    # alert_text = visible_text(alert)

    alert_text = alert_danger_text()

    assert alert_text == "Your username is invalid!"
    assert current_url() == login_index()
  end

  test "correct username, invalid password" do
    navigate_to(login_index())

    form = find_element(:id, "login")
    username = find_within_element(form, :id, "username")
    password = find_within_element(form, :id, "password")
    submit = find_within_element(form, :class, "btn-lg")

    username |> fill_field("tomsmith")
    password |> fill_field("wrong")
    submit |> click()
    take_screenshot()

    alert_text = alert_danger_text()

    assert alert_text == "Your password is invalid!"
    assert current_url() == login_index()
  end


  defp secure_index do
    secure_url(HoundPlayground.Endpoint, :index)
  end

  test "cannot access secure without logging in" do
    navigate_to(secure_index())
    take_screenshot()

    alert_text = alert_danger_text()

    assert alert_text == "You must login to view the secure area!"
    assert current_url() == login_index()
  end

  test "correct username and password" do
    navigate_to(login_index())

    form = find_element(:id, "login")
    username = find_within_element(form, :id, "username")
    password = find_within_element(form, :id, "password")
    submit = find_within_element(form, :class, "btn-lg")

    username |> fill_field("tomsmith")
    password |> fill_field("SuperSecretPassword!")
    submit |> click()
    take_screenshot()

    alert = find_element(:xpath, ~s|//p[contains(@class, 'alert-info')]|)
    alert_text = visible_text(alert)

    assert alert_text == "You logged into a secure area!"
    assert current_url() == secure_index()
  end

  # verify that when the user's session is deleted, they are redirected to the
  # login after refreshing the page. The user session is stored in cookies.
  test "clearing cookies causes logout" do
    navigate_to(login_index())

    form = find_element(:id, "login")
    username = find_within_element(form, :id, "username")
    password = find_within_element(form, :id, "password")
    submit = find_within_element(form, :class, "btn-lg")

    username |> fill_field("tomsmith")
    password |> fill_field("SuperSecretPassword!")
    submit |> click()

    delete_cookies()
    refresh_page()

    assert current_url() == login_index()
  end

end
