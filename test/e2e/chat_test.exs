defmodule HoundPlayground.ChatTest do
  use HoundPlayground.ConnCase
  use Hound.Helpers

@docp """
The hound_session statement manages the session's lifecycle for us: it
creates and ends a new session for each test, so they don’t interfere.
Without it, we would have to use start_session/1 / end_session/1 helpers in each test.
"""
hound_session()

@docp """
Everything done with Hound is executed in a session context.
Using hound_session Hound always creates a default session named :default inside
our test. If we need more than one session, we can use change_session_to/2 helper,
passing the session name, and it will be created if doesn't exist.
To go back to the default session, we use change_to_default_session/0.
"""
  test "receive chat from another session" do
    send_message("Hello from session 1")
    take_screenshot()

    change_session_to("session2")
    send_message("Message from session 2")
    take_screenshot()

    change_to_default_session()

    # extract messages from the default session from all <li> elements inside <div>
    messages = find_element(:id, "messages")
    [msg1, msg2] = messages
    |> find_all_within_element(:tag, "li")
    |> Enum.map(&inner_text/1)
    |> Enum.map(&parse_message/1)
    
    # verify that the message fro session 2 was received in the default session
    # and that they're in the correct order. Also verify the session PIDs are different
    assert msg1.text == "Hello from session 1"
    assert msg2.text == "Message from session 2"
    assert msg1.pid != msg2.pid
  end

  # helper returning the URL for the chat page
  defp chat_index do
    chat_url(HoundPlayground.Endpoint, :index)
  end

  # helper extracts the chat pid and message text
  defp parse_message(message) do
    [_match, pid, text] = Regex.run(~r/\[(.*?)\] (.*)/, message)
    %{pid: pid, text: text}
  end

@docp """
helper navigates to the chat page, locates the input field, and after
filling the input field with a message, presses the enter key.

Important: send_keys/1 also supports the "Return" key but, most of the time
that's not what you want. To imitate JavaScript keycode === 13 always use :enter.

At the end of send_message/1 there's a call to :timer.sleep/1.
The reason behind this is that there's no easy way to determine when an
asynchronous operation ends, in our case, JS communication through WebSockets.
Depending on the system's speed this might not be necessary, however, it is
safer to always wait while doing any kind of asynchronous JS.
Hound internal tests use this technique while interacting with JavaScript.
"""
  defp send_message(message) do
    navigate_to(chat_index())
    chat_input2 = find_element(:id, "chat-input")
    chat_input2 |> fill_field(message)
    send_keys(:enter)
    :timer.sleep(1000)
  end

end
