# HoundPlayground

**Following the Hound tutorial at: https://semaphoreci.com/community/tutorials/end-to-end-testing-in-elixir-with-hound**

[![Build Status](https://semaphoreci.com/api/v1/adequatedeveloper/hound_playground/branches/master/badge.svg)](https://semaphoreci.com/adequatedeveloper/hound_playground)

## Features
* End-to-end browser automation web testing in Elixir using the Hound package.
* Uses the headless browser PhantomJS with its WebDriver implementation - GhostDriver.
* Includes browser automation web testing at Semaphore CI.
* Provided links in project notes about another automation package - Wallaby.

## More Info:
* See file: /project_notes.txt.
* See file: /semaphoreCI-notes.txt

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
