
Configuring Semaphore CI for web browser automation testing:
  1. mix local.hex --force
  2. mix deps.get --only test
  3. change-phantomjs-version 2.1.1
  4. nvm install 6.9.4
  5. nvm use 6.9.4
  6. npm install
  7. node_modules/.bin/brunch build
  8. MIX_ENV=test mix local.rebar --force
  9. MIX_ENV=test mix do deps.compile, compile
  10. phantomjs --wd --webdriver-loglevel=ERROR &
